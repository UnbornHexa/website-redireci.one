/* ---- Methods ---- */

exports.receber = (usuario) => {
  const apiRoute = 'https://api-web.redireci.one/paginas/usuario'
  const url = `${apiRoute}/${usuario}`
  return url
}
