/* ---- Requires ---- */

const axios = require('axios')

/* ---- Methods ---- */

exports.get = async (url) => {
  return new Promise((resolve, reject) => {
    axios.get(url)
      .then(data => resolve(data))
      .catch(error => reject(error))
  })
}
