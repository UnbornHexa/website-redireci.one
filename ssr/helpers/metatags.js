/* ---- Requires ---- */

const cheerio = require('cheerio')

/* ---- Methods ---- */

exports.buildUsuario = async (html, usuario) => {
  const $ = cheerio.load(html)

  const titulo = `@${usuario}`
  const descricao = `Bem vindo(a) a Fast Page de @${usuario}`
  const urlCanonica = `https://redireci.one/${usuario}`

  $('title').text(titulo)
  $('meta[property="og:title"]').attr('content', titulo)
  $('meta[name="twitter:title"]').attr('content', titulo)
  $('meta[name="discord:title"]').attr('content', titulo)

  $('meta[name="description"]').attr('content', descricao)
  $('meta[itemprop="description"]').attr('content', descricao)
  $('meta[property="og:description"]').attr('content', descricao)
  $('meta[name="twitter:description"]').attr('content', descricao)
  $('meta[name="discord:description"]').attr('content', descricao)

  $('link[rel="canonical"]').attr('href', urlCanonica)
  $('meta[property="og:url"]').attr('content', urlCanonica)
  $('meta[name="discord:site"]').attr('content', urlCanonica)
  $('meta[property="discord:url"]').attr('content', urlCanonica)

  $('meta[name="author"]').attr('content', titulo)
  $('meta[itemprop="name"]').attr('content', titulo)
  $('meta[name="twitter:site"]').attr('content', titulo)
  $('meta[name="article:author"]').attr('content', titulo)
  $('meta[name="application-name"]').attr('content', titulo)
  $('meta[property="og:site_name"]').attr('content', titulo)
  $('meta[name="apple-mobile-web-app-title"]').attr('content', titulo)

  return $
}
