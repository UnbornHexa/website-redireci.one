/* ---- Requires ---- */

const express = require('express')
const router = express.Router()

/* ---- Controllers ---- */

const USUARIO_CONTROLLER = require('../controllers/usuario')

/* ---- Methods ---- */

router.get('/:usuario', USUARIO_CONTROLLER.renderizar)

module.exports = router
