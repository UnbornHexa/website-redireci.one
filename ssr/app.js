/* ---- Requires ---- */

const express = require('express')
const path = require('path')
const morgan = require('morgan')
const serveStatic = require('serve-static')
const compression = require('compression')
const helmet = require('helmet')

const app = express()
const helmetData = require('./data/helmet.json')

/* ---- Middlewares ---- */

app.use(compression())
app.use(morgan('common'))

app.use(helmet.hidePoweredBy())
app.use(helmet.contentSecurityPolicy(helmetData.contentSecurityPolicy))

app.use('/', serveStatic(path.resolve(__dirname, '../dist')))

/* ---- Routes ---- */

// Index

const INDEX_ROUTE = require('./routes/index')
app.use('/', INDEX_ROUTE)

// User

const USUARIO_ROUTE = require('./routes/usuario')
app.use('/', USUARIO_ROUTE)

// Erro

const ERRO_ROUTE = require('./routes/erro')
app.use('/', ERRO_ROUTE)

module.exports = app
