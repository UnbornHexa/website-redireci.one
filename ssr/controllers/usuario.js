/* ---- Requires ---- */

const file = require('../helpers/read-file-async')
const path = require('path')

/* ---- Helpers ---- */

const axios = require('../helpers/axios')
const urlApiWeb = require('../helpers/url-api-web')
const metatags = require('../helpers/metatags')

/* ---- Constants ---- */

const PATH_HTML = path.resolve(__dirname, '../../dist')

/* ---- Methods ---- */

exports.renderizar = async (req, res, next) => {
  const usuario = req.params.usuario
  const url = urlApiWeb.receber(usuario)

  // step 1 - check if usuario exists
  const response = await axios.get(url)
  const { body } = response.data

  if (body.length === 0) {
    next()
    return
  }

  // step 2 - build page with metatags
  const html = await file.readFileAsync(`${PATH_HTML}/usuario.html`)
  const $ = await metatags.buildUsuario(html, usuario)

  // step 3 - inject data from usuario
  const paginaDadosObj = {
    links: body[0].links,
    idPagina: body[0]._id
  }
  const paginaDadosJson = JSON.stringify(paginaDadosObj)
  const scriptDados = `<script>window.__layoutPagina = ${paginaDadosJson};</script>`
  $('body').append(scriptDados)

  res.status(200).send($.html())
}
