/* ---- Requires ---- */

const file = require('../helpers/read-file-async')
const path = require('path')

/* ---- Constants ---- */

const PATH_HTML = path.resolve(__dirname, '../../dist')

/* ---- Methods ---- */

exports.renderizar = async (req, res, next) => {
  const html = await file.readFileAsync(`${PATH_HTML}/erro.html`)
  res.status(404).send(html)
}
