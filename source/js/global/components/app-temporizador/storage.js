/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Constants ---- */

  const KEY_DATA_EXPIRACAO = 'data-expiracao'

  /* ---- Methods ---- */

  methods.receberDataExpiracao = () => {
    const timestampAgora = new Date().getTime()
    const timestamp2Horas = 1000 * 60 * 60 * 2

    const timestampStorage = methods.receberTimestampStorage() || 0
    if (timestampStorage > timestampAgora) {
      return new Date(Number(timestampStorage))
    }

    const dataExpiracao = timestampAgora + timestamp2Horas
    methods.definirTimestampStorage(dataExpiracao)
    return new Date(dataExpiracao)
  }

  /* ---- */

  methods.receberTimestampStorage = () => {
    return window.localStorage.getItem(KEY_DATA_EXPIRACAO)
  }

  methods.definirTimestampStorage = (timestamp) => {
    window.localStorage.setItem(KEY_DATA_EXPIRACAO, timestamp)
  }

  return methods
}

module.exports = Module
