/* ---- Requires ---- */

const HELPERS = require('./helpers')
const STORAGE = require('./storage')
const VIEW = require('./view')

/* ----  Module ---- */

class AppTemporizador extends window.HTMLElement {
  constructor () {
    super()
    this.__state = { dataExpiracao: null }
    this.definirDataExpiracao()
    this.renderizar()
  }

  /* ---- Methods ---- */

  // render

  renderizar () {
    this.innerHTML = `
    <div class="titulo">
      <h2 name="mensagem">Comece o primeiro mês <span>por apenas R$ 1,21</span> antes do tempo acabar!</h2>
    </div>
    <!-- itens -->
    <div class="itens">
      <div class="item">
        <div class="um">
          <h3 name="dias">00</h3>
        </div>
        <p>Dias</p>
      </div>
      <div class="item">
        <div class="dois">
          <h3 name="horas">00</h3>
        </div>
        <p>Horas</p>
      </div>
      <div class="item">
        <div class="um">
          <h3 name="minutos">00</h3>
        </div>
        <p>Minutos</p>
      </div>
      <div class="item">
        <div class="dois">
          <h3 name="segundos">00</h3>
        </div>
        <p>Segundos</p>
      </div>
    </div>
    `
  }

  // define

  definirDataExpiracao () {
    const dataExpiracao = STORAGE().receberDataExpiracao()
    this.__state.dataExpiracao = dataExpiracao
  }

  // system

  iniciar () {
    const intervalo = setInterval(() => {
      const agora = HELPERS().retornarDataAtual()
      const dataExpiracao = this.__state.dataExpiracao

      const tempoRestante = HELPERS().calcularTempoRestante(dataExpiracao, agora)
      const tempoFinalizado = { dias: 0, horas: 0, minutos: 0, segundos: 0 }

      if (tempoRestante === 0) {
        VIEW().atualizarLayout(this, tempoFinalizado)
        clearInterval(intervalo)
        return
      }

      VIEW().atualizarLayout(this, tempoRestante)
    }, 1000)
  }
}

module.exports = AppTemporizador
