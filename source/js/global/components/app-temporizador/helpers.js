/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Methods ---- */

  methods.retornarDataAtual = () => {
    return new Date()
  }

  methods.calcularTempoRestante = (data1, data2) => {
    const diferenca = data1 - data2
    if (diferenca < 0) return 0

    const dias = Math.floor(diferenca / (1000 * 60 * 60 * 24))
    const horas = Math.floor((diferenca % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60))
    const minutos = Math.floor((diferenca % (1000 * 60 * 60)) / (1000 * 60))
    const segundos = Math.floor((diferenca % (1000 * 60)) / 1000)

    return { dias, horas, minutos, segundos }
  }

  return methods
}

module.exports = Module
