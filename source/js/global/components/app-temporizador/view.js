/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Methods ---- */

  methods.atualizarLayout = (componente, tempoRestante) => {
    componente.querySelector('h3[name="dias"]').innerText = tempoRestante.dias
    componente.querySelector('h3[name="horas"]').innerText = tempoRestante.horas
    componente.querySelector('h3[name="minutos"]').innerText = tempoRestante.minutos
    componente.querySelector('h3[name="segundos"]').innerText = tempoRestante.segundos
  }

  return methods
}

module.exports = Module
