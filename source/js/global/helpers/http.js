/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Constants ---- */

  const __headers = {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  }

  /* ---- Methods ---- */

  methods.get = (url, headers, callback) => {
    const fetchOption = {
      method: 'GET',
      headers: headers || __headers
    }
    methods._request(url, fetchOption, callback)
  }

  methods.post = (url, data, headers, callback) => {
    const fetchOption = {
      method: 'POST',
      body: JSON.stringify(data) || JSON.stringify({}),
      headers: headers || __headers
    }
    methods._request(url, fetchOption, callback)
  }

  methods.put = (url, data, headers, callback) => {
    const fetchOption = {
      method: 'PUT',
      body: JSON.stringify(data) || JSON.stringify({}),
      headers: headers || __headers
    }
    methods._request(url, fetchOption, callback)
  }

  methods.delete = (url, headers, callback) => {
    const fetchOption = {
      method: 'DELETE',
      headers: headers || __headers
    }
    methods._request(url, fetchOption, callback)
  }

  methods.patch = (url, data, headers, callback) => {
    const fetchOption = {
      method: 'PATCH',
      body: JSON.stringify(data) || JSON.stringify({}),
      headers: headers || __headers
    }
    methods._request(url, fetchOption, callback)
  }

  methods.postFile = (url, data, headers, callback) => {
    const fetchOption = {
      method: 'POST',
      body: data,
      headers: headers
    }
    methods._request(url, fetchOption, callback)
  }

  // General

  methods._request = (url, fetchOption, callback) => {
    fetchOption = (fetchOption === null) ? {} : fetchOption

    window.fetch(url, fetchOption)
      .then(response => {
        response.json()
          .then(data => {
            const result = {
              status: response.status,
              body: data
            }
            callback(result)
          })
      })
      .catch(error => callback(error))
  }

  // Custom Headers

  methods.setHeadersWithToken = (token) => {
    const headers = {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      'x-access-token': token
    }
    return headers
  }

  methods.setHeadersWithTokenToSendFile = (token) => {
    const headers = { 'x-access-token': token }
    return headers
  }

  return methods
}

module.exports = Module
