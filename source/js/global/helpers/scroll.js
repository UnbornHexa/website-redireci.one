/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Methods ---- */

  methods.rolarAteSeletor = (seletor) => {
    const elemento = document.querySelector(seletor)

    window.scrollTo({
      top: elemento.offsetTop,
      left: 0,
      behavior: 'smooth'
    })
  }

  return methods
}

module.exports = Module
