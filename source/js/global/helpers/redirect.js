/* ---- Requires ---- */

const HOSTS = require('../data/hosts')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Methods ---- */

  methods.paginaInterna = (pagina) => {
    const url = `${HOSTS().url.SSR_WEBSITE}/${pagina}`
    window.location.assign(url)
  }

  methods.urlExterna = (url) => {
    if (!url) return

    const a = document.createElement('a')
    a.rel = 'noopener noreferrer nofollow'
    a.target = '_blank'
    a.href = url
    a.click()
  }

  return methods
}

module.exports = Module
