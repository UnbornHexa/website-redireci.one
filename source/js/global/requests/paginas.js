/* ---- Requires ---- */

const HOSTS = require('../data/hosts')
const HTTP = require('../helpers/http')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Constants ---- */

  const URL = `${HOSTS().url.API_WEBSITE}/paginas`

  /* ---- Methods ---- */

  methods.incrementarVisualizacoes = (idPagina) => {
    const url = `${URL}/incrementar/visualizacoes/${idPagina}`

    return new Promise((resolve, reject) => {
      HTTP().patch(url, {}, null, (response) => {
        const { body } = response.body
        if (response.status === 200) return resolve(body)

        reject(new Error(response.body))
      })
    })
  }

  return methods
}

module.exports = Module
