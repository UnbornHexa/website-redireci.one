/* ---- Requires ---- */

const HELPER_SCROLL = require('../helpers/scroll')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Methods ---- */

  methods.habilitarCliqueRolarAteSeletor = () => {
    window.addEventListener('click', (evento) => callbackRolarAteSeletor(evento))
  }

  /* ---- Callbacks ---- */

  function callbackRolarAteSeletor (evento) {
    const seletor = evento.target.getAttribute('data-scroll-seletor')
    if (!seletor) return

    HELPER_SCROLL().rolarAteSeletor(seletor)
  }

  return methods
}

module.exports = Module
