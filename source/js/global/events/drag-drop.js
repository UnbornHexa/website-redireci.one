/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Methods ---- */

  methods.habilitarBloqueioDragAndDrop = () => {
    document.addEventListener('dragstart', (evento) => callbackDragDrop(evento), false)
    document.addEventListener('drop', (evento) => callbackDragDrop(evento), false)
  }

  /* ---- Callbacks ---- */

  function callbackDragDrop (evento) {
    evento.preventDefault()
  }

  return methods
}

module.exports = Module
