/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Methods ---- */

  methods.habilitarCliqueCollapsible = () => {
    window.addEventListener('click', (evento) => callbackCliqueCollapsible(evento))
  }

  /* ---- Callbacks ---- */

  function callbackCliqueCollapsible (evento) {
    const elemento = 'div[name="collapsible"]'
    if (!evento.target.matches(elemento)) return

    const divResposta = evento.target.querySelector('div[name="resposta"]')
    divResposta.classList.toggle('aberto')
  }

  return methods
}

module.exports = Module
