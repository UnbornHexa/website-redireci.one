/* ---- Requires ---- */

const EVENT_DRAG_DROP = require('./drag-drop')
const EVENT_COLLAPSIBLE = require('./collapsible')
const EVENT_LAZY_LOADING = require('./lazy-loading')
const EVENT_LOADING = require('./loading')
const EVENT_REDIRECIONAMENTO = require('./redirecionamento')
const EVENT_SCROLL_SELETOR = require('./scroll-seletor')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Methods ---- */

  methods.habilitarTodosOsEventosGlobais = () => {
    EVENT_LOADING().habilitarLoading()

    EVENT_DRAG_DROP().habilitarBloqueioDragAndDrop()
    EVENT_LAZY_LOADING().habilitarLazyLoading()
    EVENT_SCROLL_SELETOR().habilitarCliqueRolarAteSeletor()

    EVENT_REDIRECIONAMENTO().habilitarCliquePaginaInterna()
    EVENT_REDIRECIONAMENTO().habilitarCliqueUrlExterna()

    EVENT_COLLAPSIBLE().habilitarCliqueCollapsible()
  }

  return methods
}

module.exports = Module
