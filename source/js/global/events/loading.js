/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Elements ---- */

  const body = document.querySelector('body')
  const loading = document.querySelector('section[name="loading"]')

  /* ---- Methods ---- */

  methods.habilitarLoading = () => {
    window.addEventListener('load', () => callbackLoading())
  }

  /* ---- Callbacks ---- */

  function callbackLoading () {
    setTimeout(() => {
      body.classList.remove('carregando')
      loading.classList.add('ocultar')
    }, 500)
  }

  return methods
}

module.exports = Module
