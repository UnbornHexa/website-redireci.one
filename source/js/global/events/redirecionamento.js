/* ---- Requires ---- */

const HELPER_REDIRECT = require('../helpers/redirect')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Methods ---- */

  methods.habilitarCliquePaginaInterna = () => {
    window.addEventListener('click', (evento) => callbackPaginaInterna(evento))
  }

  methods.habilitarCliqueUrlExterna = () => {
    window.addEventListener('click', (evento) => callbackUrlExterna(evento))
  }

  /* ---- Callbacks ---- */

  function callbackPaginaInterna (evento) {
    const paginaInterna = evento.target.getAttribute('data-pagina-interna')
    if (!paginaInterna) return

    HELPER_REDIRECT().paginaInterna(paginaInterna)
  }

  function callbackUrlExterna (evento) {
    const urlExterna = evento.target.getAttribute('data-url-externa')
    if (!urlExterna) return

    HELPER_REDIRECT().urlExterna(urlExterna)
  }

  return methods
}

module.exports = Module
