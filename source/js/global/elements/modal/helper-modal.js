/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Elements ---- */

  const body = document.querySelector('body')

  /* ---- Constants ---- */

  const CLASSE_BODY = 'bloqueado'
  const CLASSE_MODAL = 'mostrar'

  /* ---- Methods ---- */

  methods.abrirModal = (modal) => {
    body.classList.add(CLASSE_BODY)
    modal.classList.add(CLASSE_MODAL)
  }

  methods.fecharModal = (modal) => {
    body.classList.remove(CLASSE_BODY)
    modal.classList.remove(CLASSE_MODAL)
  }

  return methods
}

module.exports = Module
