/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Constants ---- */

  const PROTOCOL = window.location.protocol
  const HOST = window.location.host

  /* ---- Methods ---- */

  methods.url = {
    SSR_WEBSITE: `${PROTOCOL}//${HOST}`,

    API_WEBSITE: 'https://api-web.redireci.one'
    // API_WEBSITE: 'http://localhost:53101'
  }

  return methods
}

module.exports = Module
