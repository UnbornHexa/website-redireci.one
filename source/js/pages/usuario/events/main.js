/* ---- Requires ---- */

const VIEW_TOPO = require('../views/topo')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Methods ---- */

  methods.resizeWindow = () => {
    window.addEventListener('resize', callbackResizeWindow)
  }

  /* ---- Callbacks ---- */

  function callbackResizeWindow () {
    VIEW_TOPO().definirContadorAtual(1)
    const main = document.querySelector('main')
    main.scrollLeft = 0
  }

  return methods
}

module.exports = Module
