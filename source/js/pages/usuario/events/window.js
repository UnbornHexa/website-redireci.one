/* ---- Requires ---- */

const MONTAR_PAGINA = require('../helpers/montar-pagina')
const REQUEST_PAGINAS = require('../../../global/requests/paginas')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Methods ---- */

  methods.povoarPagina = () => {
    window.addEventListener('load', () => callbackPovoarPagina())
  }

  methods.incrementarVisualizacoes = () => {
    window.addEventListener('load', () => callbackIncrementarVisualizacoes())
  }

  /* ---- Callbacks ---- */

  function callbackPovoarPagina () {
    const dadosPagina = window.__layoutPagina || []
    const links = dadosPagina.links
    MONTAR_PAGINA().montar(links)
  }

  function callbackIncrementarVisualizacoes () {
    const idPagina = window.__layoutPagina.idPagina
    if (!idPagina) return
    REQUEST_PAGINAS().incrementarVisualizacoes(idPagina)
  }

  return methods
}

module.exports = Module
