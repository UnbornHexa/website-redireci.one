/* ---- Requires ---- */

const EVENT = require('../../../global/events/events')
const EVENT_MAIN = require('./main')
const EVENT_SETAS = require('./setas')
const EVENT_WINDOW = require('./window')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Methods ---- */

  methods.habilitarTodosOsEventos = () => {
    EVENT_WINDOW().povoarPagina()
    EVENT().habilitarTodosOsEventosGlobais()

    EVENT_MAIN().resizeWindow()
    EVENT_SETAS().scrollSetas()
    EVENT_SETAS().touchSetas()

    EVENT_WINDOW().incrementarVisualizacoes()
  }

  return methods
}

module.exports = Module
