/* ---- Requires ---- */

const VIEW_TOPO = require('../views/topo')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Variables ---- */

  let xDown = null
  let yDown = null
  let rolagemTravada = false

  /* ---- Elements ---- */

  const main = document.querySelector('main')
  const divTopo = document.querySelector('div[name="topo"]')
  const buttonAvancar = divTopo.querySelector('button[name="avancar"]')
  const buttonVoltar = divTopo.querySelector('button[name="voltar"]')

  /* ---- Methods ---- */

  methods.scrollSetas = () => {
    buttonAvancar.addEventListener('click', () => callbackScrollSetaAvancar())
    buttonVoltar.addEventListener('click', () => callbackScrollSetaVoltar())
  }

  methods.touchSetas = () => {
    main.addEventListener('touchstart', (evento) => callbackTouchStart(evento))
    main.addEventListener('touchmove', (evento) => callbackTouchMove(evento))
  }

  /* ---- Callbacks ---- */

  // Click

  function callbackScrollSetaAvancar () {
    if (rolagemTravada) return
    travarRolagem()

    const coordenadaX = main.scrollLeft + main.offsetWidth
    rolar(coordenadaX)

    const contadorAtual = VIEW_TOPO().retornarContadorAtual()
    const contadorTotal = VIEW_TOPO().retornarContadorTotal()
    if (contadorAtual >= contadorTotal) return

    VIEW_TOPO().definirContadorAtual(contadorAtual + 1)
  }

  function callbackScrollSetaVoltar () {
    if (rolagemTravada) return
    travarRolagem()

    const coordenadaX = main.scrollLeft - main.offsetWidth
    rolar(coordenadaX)

    const contadorAtual = VIEW_TOPO().retornarContadorAtual()
    if (contadorAtual <= 1) return

    VIEW_TOPO().definirContadorAtual(contadorAtual - 1)
  }

  // Touch

  function callbackTouchStart (evento) {
    const firstTouch = evento.touches[0]
    xDown = firstTouch.clientX
    yDown = firstTouch.clientY
  }

  function callbackTouchMove (evento) {
    if (!xDown || !yDown) return

    const xUp = evento.touches[0].clientX
    const yUp = evento.touches[0].clientY

    const xDiff = xDown - xUp
    const yDiff = yDown - yUp

    if (Math.abs(xDiff) < Math.abs(yDiff)) return

    if (xDiff > 0) callbackScrollSetaAvancar()
    else callbackScrollSetaVoltar()

    xDown = null
    yDown = null
  }

  /* ---- Aux Functions ---- */

  function rolar (coordenadaX) {
    main.scrollTo({
      left: coordenadaX,
      behavior: 'smooth'
    })
  }

  function travarRolagem () {
    rolagemTravada = true
    setTimeout(() => { rolagemTravada = false }, 500)
  }

  return methods
}

module.exports = Module
