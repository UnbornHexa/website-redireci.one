/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Methods ---- */

  methods.renderizar = () => {
    const section = document.createElement('section')

    const divItens = document.createElement('div')
    divItens.classList.add('itens')

    const divIcone = document.createElement('div')
    divIcone.classList.add('icone')

    const imgIcone = document.createElement('img')
    imgIcone.setAttribute('name', 'icone')

    const divDescricao = document.createElement('div')
    divDescricao.classList.add('descricao')

    const pDescricao = document.createElement('p')
    pDescricao.setAttribute('name', 'descricao')

    const divBotao = document.createElement('div')
    divBotao.classList.add('botao')

    const buttonBotao = document.createElement('button')
    buttonBotao.setAttribute('name', 'botao')

    // -----

    divIcone.appendChild(imgIcone)
    divDescricao.appendChild(pDescricao)
    divBotao.appendChild(buttonBotao)

    divItens.appendChild(divIcone)
    divItens.appendChild(divDescricao)
    divItens.appendChild(divBotao)

    section.appendChild(divItens)

    return section
  }

  return methods
}

module.exports = Module
