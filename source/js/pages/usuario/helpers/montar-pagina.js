/* ---- Requires ---- */

const RENDERIZAR_SECTION = require('./renderizar-section')
const APLICAR_LAYOUT = require('./aplicar-layout-section')
const VIEW_TOPO = require('../views/topo')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Elements ---- */

  const main = document.querySelector('main')

  /* ---- Methods ---- */

  methods.montar = (links) => {
    const linksValidos = removerLinksDeletados(links)
    const linksOrdenados = ordenarPosicionamento(linksValidos)

    for (const link of linksOrdenados) {
      montarPagina(link)
    }

    povoarContadorTopo(linksOrdenados.length)
  }

  /* ---- Aux Functions ---- */

  function removerLinksDeletados (links) {
    const filtro = (link) => (link.deletado === false)
    return links.filter(filtro)
  }

  function ordenarPosicionamento (links) {
    const ordem = (link1, link2) => (link1.posicao - link2.posicao)
    return links.sort(ordem)
  }

  function montarPagina (pagina) {
    const dadosBotao = pagina.botao
    const dadosDescricao = pagina.descricao
    const dadosFundo = pagina.fundo
    const dadosIcone = pagina.icone

    const sectionRenderizada = RENDERIZAR_SECTION().renderizar()
    const buttonBotao = sectionRenderizada.querySelector('button[name="botao"]')
    const pDescricao = sectionRenderizada.querySelector('p[name="descricao"]')
    const imgIcone = sectionRenderizada.querySelector('img[name="icone"]')

    APLICAR_LAYOUT().carregarBotao(buttonBotao, dadosBotao)
    APLICAR_LAYOUT().carregarDescricao(pDescricao, dadosDescricao)
    APLICAR_LAYOUT().carregarFundo(sectionRenderizada, dadosFundo)
    APLICAR_LAYOUT().carregarIcone(imgIcone, dadosIcone)

    const divLinks = main.querySelector('.links')
    divLinks.appendChild(sectionRenderizada)
  }

  function povoarContadorTopo (contadorTotal) {
    VIEW_TOPO().definirContadorAtual(1)
    VIEW_TOPO().definirContadorTotal(contadorTotal)
  }

  return methods
}

module.exports = Module
