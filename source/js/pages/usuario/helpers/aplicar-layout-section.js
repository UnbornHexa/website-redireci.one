/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Methods ---- */

  methods.carregarBotao = (elemento, dados) => {
    elemento.style.color = dados.corTexto
    elemento.style.backgroundColor = dados.corFundo

    const classeFormato = `formato_botao_${dados.formato}`
    elemento.classList.add(classeFormato)

    const classFonte = `fonte_${dados.fonte}`
    elemento.classList.add(classFonte)

    elemento.innerText = dados.texto
    elemento.setAttribute('data-url-externa', dados.url)
  }

  methods.carregarDescricao = (elemento, dados) => {
    const classFonte = `fonte_${dados.fonte}`
    elemento.classList.add(classFonte)

    elemento.style.color = dados.cor
    elemento.innerText = dados.texto
  }

  methods.carregarIcone = (elemento, dados) => {
    const classeFormato = `formato_icone_${dados.formato}`
    elemento.classList.add(classeFormato)

    elemento.src = dados.imagem
  }

  // Fundo

  methods.carregarFundo = (elemento, dados) => {
    const fundoTipoSelecionado = dados.selecionado
    if (fundoTipoSelecionado === 'cor') carregarFundoCor(elemento, dados)
    else if (fundoTipoSelecionado === 'gradiente') carregarFundoGradiente(elemento, dados)
    else if (fundoTipoSelecionado === 'imagem') carregarFundoImagem(elemento, dados)
  }

  /* ---- Aux Functions ---- */

  function carregarFundoCor (elemento, dados) {
    elemento.style.backgroundColor = dados.cor
  }

  function carregarFundoGradiente (elemento, dados) {
    const cor1 = dados.gradiente.cor1
    const cor2 = dados.gradiente.cor2
    elemento.style.backgroundImage = `linear-gradient(${cor1}, ${cor2})`
  }

  function carregarFundoImagem (elemento, dados) {
    elemento.style.backgroundImage = `url(${dados.imagem})`
  }

  return methods
}

module.exports = Module
