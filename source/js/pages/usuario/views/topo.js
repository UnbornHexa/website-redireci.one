/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Elements ---- */

  const divTopo = document.querySelector('div[name="topo"]')
  const spanContadorAtual = divTopo.querySelector('span[name="atual"]')
  const spanContadorTotal = divTopo.querySelector('span[name="total"]')

  /* ---- Methods ---- */

  methods.definirContadorTotal = (numero) => {
    spanContadorTotal.innerText = numero
  }

  methods.definirContadorAtual = (numero = 1) => {
    spanContadorAtual.innerText = numero
  }

  methods.retornarContadorAtual = () => {
    const contador = spanContadorAtual.innerText || 1
    return Number(contador)
  }

  methods.retornarContadorTotal = () => {
    const contador = spanContadorTotal.innerText || 1
    return Number(contador)
  }

  return methods
}

module.exports = Module
