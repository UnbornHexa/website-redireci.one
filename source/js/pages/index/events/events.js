/* ---- Requires ---- */

const EVENT = require('../../../global/events/events')
const EVENT_CELULAR = require('./celular')
const EVENT_CHAPORT = require('./chaport')
const EVENT_MODAL_ASSINAR = require('./modal-assinar')
const EVENT_MODAL_HEADER = require('./modal-header')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Methods ---- */

  methods.habilitarTodosOsEventos = () => {
    EVENT().habilitarTodosOsEventosGlobais()

    EVENT_CHAPORT().habilitarCliqueChat()
    EVENT_CELULAR().habilitarCliqueTrocarImagemCelular()

    EVENT_MODAL_HEADER().habilitarCliqueAbrirModal()
    EVENT_MODAL_HEADER().habilitarCliqueFecharModal()

    EVENT_MODAL_ASSINAR().habilitarCliqueAbrirModal()
    EVENT_MODAL_ASSINAR().habilitarCliqueFecharModal()
  }

  return methods
}

module.exports = Module
