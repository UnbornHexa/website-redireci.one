/* ---- Requires ---- */

const VIEW_MODAL = require('../views/modal-assinar')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Elements ---- */

  const section = document.querySelector('section[name="assinar"]')
  const botaoAbrir = section.querySelector('img[name="abrir_modal"]')
  const botaoFechar = section.querySelector('button[name="fechar_modal"]')

  /* ---- Methods ---- */

  methods.habilitarCliqueAbrirModal = () => {
    botaoAbrir.addEventListener('click', () => callbackAbrirModal())
  }

  methods.habilitarCliqueFecharModal = () => {
    botaoFechar.addEventListener('click', () => callbackFecharModal())
  }

  /* ---- Callbacks ---- */

  function callbackAbrirModal () {
    VIEW_MODAL().mostrar()
  }

  function callbackFecharModal () {
    VIEW_MODAL().ocultar()
  }

  return methods
}

module.exports = Module
