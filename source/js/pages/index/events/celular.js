/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Constants ---- */

  const IMAGENS = [
    'https://static.redireci.one/assets/web/imagem-preview-1.jpg',
    'https://static.redireci.one/assets/web/imagem-preview-2.jpg',
    'https://static.redireci.one/assets/web/imagem-preview-3.jpg',
    'https://static.redireci.one/assets/web/imagem-preview-4.jpg'
  ]

  /* ---- Elements ---- */

  const section = document.querySelector('section[name="descricao"]')
  const img = section.querySelector('img')
  const divImagemCelular = section.querySelector('div[name="imagem-celular"]')

  /* ---- Methods ---- */

  methods.habilitarCliqueTrocarImagemCelular = () => {
    img.addEventListener('click', () => callbackTrocarImagemCelular())
  }

  /* ---- Callbacks ---- */

  function callbackTrocarImagemCelular () {
    const idImagem = Number(divImagemCelular.getAttribute('data-id'))
    const idImagemProxima = (IMAGENS.length > idImagem)
      ? idImagem + 1
      : 1

    divImagemCelular.setAttribute('data-id', idImagemProxima)
    divImagemCelular.style.backgroundImage = `url(${IMAGENS[idImagemProxima - 1]})`
  }

  return methods
}

module.exports = Module
