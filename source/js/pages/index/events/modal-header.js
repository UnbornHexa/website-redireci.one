/* ---- Requires ---- */

const VIEW_MODAL = require('../views/modal-header')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Elements ---- */

  const header = document.querySelector('header')
  const botaoAbrir = header.querySelector('img[name="abrir_modal"]')
  const botaoFechar = header.querySelector('button[name="fechar_modal"]')

  /* ---- Methods ---- */

  methods.habilitarCliqueAbrirModal = () => {
    botaoAbrir.addEventListener('click', () => callbackAbrirModal())
  }

  methods.habilitarCliqueFecharModal = () => {
    botaoFechar.addEventListener('click', () => callbackFecharModal())
  }

  /* ---- Callbacks ---- */

  function callbackAbrirModal () {
    VIEW_MODAL().mostrar()
  }

  function callbackFecharModal () {
    VIEW_MODAL().ocultar()
  }

  return methods
}

module.exports = Module
