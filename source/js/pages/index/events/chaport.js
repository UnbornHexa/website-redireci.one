/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Elements ---- */

  const buttonChat = document.querySelector('button[name="chaport"]')

  /* ---- Methods ---- */

  methods.habilitarCliqueChat = () => {
    buttonChat.addEventListener('click', () => {
      const chaportLauncher = document.querySelector('.chaport-container .chaport-launcher .chaport-launcher-button')
      chaportLauncher.click()
    })
  }

  return methods
}

module.exports = Module
