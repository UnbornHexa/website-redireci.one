/* ---- Polyfils ---- */

const smoothscroll = require('smoothscroll-polyfill')
smoothscroll.polyfill()

/* ---- Requires ---- */

const EVENTS = require('./events/events')

/* ---- Start ---- */

EVENTS().habilitarTodosOsEventos()
