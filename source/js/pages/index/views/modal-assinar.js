/* ---- Requires ---- */

const HELPER_MODAL = require('../../../global/elements/modal/helper-modal')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Elements ---- */

  const section = document.querySelector('section[name="assinar"]')
  const modal = section.querySelector('section[name="modal"]')
  const iframe = modal.querySelector('iframe')

  /* ---- Methods ---- */

  methods.mostrar = () => {
    methods.importarDados()
    HELPER_MODAL().abrirModal(modal)
  }

  methods.ocultar = () => {
    methods.limparCampos()
    HELPER_MODAL().fecharModal(modal)
  }

  methods.limparCampos = () => {
    iframe.src = ''
  }

  methods.importarDados = () => {
    const url = 'https://www.youtube.com/embed/05XRr-dRcVU'
    const parameters = '?autoplay=1&controls=0&disablekb=1&modestbranding=1&rel=1&showinfo=0'
    iframe.src = `${url}${parameters}`
  }

  return methods
}

module.exports = Module
