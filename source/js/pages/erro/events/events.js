/* ---- Requires ---- */

const EVENT = require('../../../global/events/events')

/* ---- Module ---- */

const Module = () => {
  const methods = {}

  /* ---- Methods ---- */

  methods.habilitarTodosOsEventos = () => {
    EVENT().habilitarTodosOsEventosGlobais()
  }

  return methods
}

module.exports = Module
