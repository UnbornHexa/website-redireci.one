/* Requires */

const webpack = require('webpack')
const path = require('path')
const modoDev = process.env.NODE_ENV !== 'production'
const MiniCSSExtractPlugin = require('mini-css-extract-plugin')
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin')
const TerserPlugin = require('terser-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const CopyPlugin = require('copy-webpack-plugin')
const BrowserSyncPlugin = require('browser-sync-webpack-plugin')

/* Modulo */

module.exports = {
  mode: modoDev ? 'development' : 'production',
  stats: {
    assets: false,
    children: false,
    chunks: false,
    modules: false,
    entrypoints: false
  },

  /* Entradas */
  entry: {
    index: path.resolve(__dirname, './webpack/index.js'),
    erro: path.resolve(__dirname, './webpack/erro.js'),
    usuario: path.resolve(__dirname, './webpack/usuario.js')
  },

  /* Saida */
  output: {
    filename: 'assets/js/[name]-[contenthash:8].js',
    path: path.resolve(__dirname, 'dist'),
    publicPath: '/'
  },

  /* Otimizacoes */
  optimization: {
    splitChunks: {
      chunks: 'all'
    },
    minimizer: [
      new TerserPlugin({
        parallel: true,
        terserOptions: { ecma: 6 }
      }),
      new OptimizeCSSAssetsPlugin()
    ]
  },

  /* Plugins */
  plugins: [
    new webpack.ProgressPlugin(),
    new BrowserSyncPlugin({
      host: 'localhost',
      port: 8080,
      proxy: 'http://localhost:53201/'
    }),
    /* Public */
    new CopyPlugin({
      patterns: [
        {
          from: path.resolve(__dirname, 'source/public/'),
          to: '[name][ext]'
        }
      ]
    }),
    /* Paginas */
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, './source/html/pages/index/index.hbs'),
      filename: 'index.html',
      chunks: ['index']
    }),
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, './source/html/pages/erro/erro.hbs'),
      filename: 'erro.html',
      chunks: ['erro']
    }),
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, './source/html/pages/usuario/usuario.hbs'),
      filename: 'usuario.html',
      chunks: ['usuario']
    }),
    /* CSS */
    new MiniCSSExtractPlugin({
      filename: 'assets/css/[name]-[contenthash:8].css'
    })
  ],

  /* Carregadores */
  module: {
    rules: [
      /* JS */
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env'],
            cacheDirectory: true
          }
        }
      },
      /* CSS */
      {
        test: /\.css$/,
        use: [
          MiniCSSExtractPlugin.loader,
          'css-loader',
          {
            loader: 'postcss-loader',
            options: {
              postcssOptions: {
                plugins: [
                  ['postcss-import', {}],
                  ['postcss-preset-env', {}],
                  ['autoprefixer']
                ]
              }
            }
          }
        ]
      },
      /* HTML */
      {
        test: /\.hbs$/,
        use: [
          {
            loader: 'handlebars-loader',
            options: {
              partialDirs: path.resolve(__dirname, './source/html/partials')
            }
          }
        ]
      }
    ]
  }
}
